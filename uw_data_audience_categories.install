<?php

/**
 * @file
 * Code for the Import Audience Categories with term lock.
 */

/**
 * Helper function to dynamically get the tid from the term_name.
 *
 * Copied from http://api.drupal.org/api/drupal/modules--taxonomy--taxonomy.module/function/taxonomy_get_term_by_name/7
 * A potentially better solution is posted there as well, but you need to know your vid in advance - Kris (2011-11-18).
 *
 * @param $term_name
 *   Term name.
 * @param $vocabulary_name
 *   Name of the vocabulary to search the term in.
 *
 * @return int|false
 *   Term id of the found term or else FALSE.
 */
function _get_audience_term_from_name($term_name, $vocabulary_name) {
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name)) {
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term) {
      if ($term->name == $term_name) {
        return $term->tid;
      }
    }
  }
  return FALSE;
}

/**
 * Implements hook_install().
 */
function uw_data_audience_categories_install() {
  watchdog('uw_data_audience_categories', 'install hook executing');
  $uwaterloo_audience_vocabulary = taxonomy_vocabulary_machine_name_load('uwaterloo_audience');

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current students.
  $tid = _get_audience_term_from_name("Current students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    // Add terms to the vocabulary we just created.
    $term = array(
      'name' => t('Current students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 0,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object_current_students = (object) $term;
    taxonomy_term_save($term_object_current_students);
  }
  else {
    // Create the term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        ' 0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);
    watchdog('uw_data_audience_categories', 'term Current students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current undergraduate students.
  $tid = _get_audience_term_from_name("Current undergraduate students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Current undergraduate students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 0,
      'parent' => $term_object_current_students->tid,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );
    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Current undergraduate students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current graduate students.
  $tid = _get_audience_term_from_name("Current graduate students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Current graduate students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 1,
      'parent' => $term_object_current_students->tid,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);
  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Current graduate students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future students.
  $tid = _get_audience_term_from_name("Future students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Future students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 2,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object_future_students = (object) $term;
    taxonomy_term_save($term_object_future_students);

  }
  else {

    // Create the Future students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Future students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future undergraduate students.
  $tid = _get_audience_term_from_name("Future undergraduate students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Future undergraduate students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 0,
      'parent' => $term_object_future_students->tid,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Future undergraduate students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future graduate students.
  $tid = _get_audience_term_from_name("Future graduate students", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Future graduate students'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 1,
      'parent' => $term_object_future_students->tid,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Future graduate students already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Faculty.
  $tid = _get_audience_term_from_name("Faculty", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Faculty'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 6,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Faculty already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Staff.
  $tid = _get_audience_term_from_name("Staff", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Staff'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 7,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Staff already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Alumni.
  $tid = _get_audience_term_from_name("Alumni", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Alumni'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 8,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Alumni already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Parents.
  $tid = _get_audience_term_from_name("Parents", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Parents'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 9,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Parents already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Donors | Friends | Supporters.
  $tid = _get_audience_term_from_name("Donors | Friends | Supporters", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Donors | Friends | Supporters'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 10,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Donors \| Friends \| Supporters already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Employers.
  $tid = _get_audience_term_from_name("Employers", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Employers'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 11,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Employers already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for International.
  $tid = _get_audience_term_from_name("International", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('International'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 12,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term International already exists');
  }

  // Check if term already exists and if not create it.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Media.
  $tid = _get_audience_term_from_name("Media", "uwaterloo_audience");

  // Only proceed if the term was not found.
  if (!$tid) {

    $term = array(
      'name' => t('Media'),
      'vid' => $uwaterloo_audience_vocabulary->vid,
      'weight' => 13,
      'vocabulary_machine_name' => 'uwaterloo_audience',
      'field_term_lock' => array(
        'und' => array(
          '0' => array(
            'value' => 1,
          ),
        ),
      ),
    );

    $term_object = (object) $term;
    taxonomy_term_save($term_object);

  }
  else {

    // Create the Undergraduate Students term object.
    $term_object = taxonomy_term_load($tid);

    // Lock the term.
    $term_object->field_term_lock = array(
      'und' => array(
        '0' => array(
          'value' => 1,
        ),
      ),
    );

    // Save the term object.
    taxonomy_term_save($term_object);

    watchdog('uw_data_audience_categories', 'term Media already exists');
  }

}

/**
 * Implements hook_uninstall().
 */
function uw_data_audience_categories_uninstall() {
  $tid = FALSE;
  $tid = _get_audience_term_from_name('Current students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Current undergraduate students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Current graduate students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Future students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Future undergraduate students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Future graduate students', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Faculty', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Staff', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Alumni', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Employers', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Parents', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Donors | Friends | Supporters', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('International', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  $tid = FALSE;
  $tid = _get_audience_term_from_name('Media', 'uwaterloo_audience');

  if ($tid) {
    taxonomy_term_delete($tid);
  }

  watchdog('uw_data_audience_categories', 'deleted taxonomy terms');
}

/**
 * Implements hook_update_N().
 */

/**
 * Replace various category names.
 *
 * Replace 'Current Students' with 'Current students',
 * 'Current Undergraduate Students' with 'Current undergraduate students',
 * 'Current Graduate Students' with 'Current graduate students',
 * 'Future Students' with 'Future students',
 * 'Future Undergraduate Students' with 'Future undergraduate students',
 * 'Future Graduate Students' with 'Future graduate students'.
 */
function uw_data_audience_categories_update_7001() {

  // Replace 'Current Students' with 'Current students'
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current Students.
  $tid = _get_audience_term_from_name("Current Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Current students';
    taxonomy_term_save($term);
  }

  // Replace 'Current Undergraduate Students' with 'Current undergraduate students'.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current Undergraduate Students.
  $tid = _get_audience_term_from_name("Current Undergraduate Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Current undergraduate students';
    taxonomy_term_save($term);
  }

  // Replace 'Current Graduate Students' with 'Current graduate students'.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Current Graduate Students.
  $tid = _get_audience_term_from_name("Current Graduate Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Current graduate students';
    taxonomy_term_save($term);
  }

  // Replace 'Future Students' with 'Future students'
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future Students.
  $tid = _get_audience_term_from_name("Future Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Future students';
    taxonomy_term_save($term);
  }

  // Replace 'Future Undergraduate Students' with 'Future undergraduate students'.
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future Undergraduate Students.
  $tid = _get_audience_term_from_name("Future Undergraduate Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Future undergraduate students';
    taxonomy_term_save($term);
  }

  // Replace 'Future Graduate Students' with 'Future graduate students'
  // Set $tid to FALSE.
  $tid = FALSE;

  // Retrieve the term id for Future Graduate Students.
  $tid = _get_audience_term_from_name("Future Graduate Students", "uwaterloo_audience");

  // Check if term already exists and if it exists, then replace term name.
  if ($tid) {
    $term = taxonomy_term_load($tid);
    $term->name = 'Future graduate students';
    taxonomy_term_save($term);
  }

}

/**
 * The default audience terms are supposed to be locked.
 * Make sure the default audience terms are locked.
 */
function uw_data_audience_categories_update_7100() {

  // Get all default audience terms which should be locked.
  $all_default_term_ids = _uw_data_audience_categories_get_locked_terms();

  /* If the default audience terms are existing, we will do
   * Step 1: check if the records related to uwaterloo_audience are existing in field_data_field_term_lock
   * Step 2: if has record(s), delete first.
   * Step 3: Insert field_data_field_term_lock table
   */
  if (!empty($all_default_term_ids) && db_table_exists('field_data_field_term_lock')) {

    // Step 1.
    $all_audience_terms_from_data_table = db_select('field_data_field_term_lock', 'l')
      ->fields('l')
      ->condition('bundle', 'uwaterloo_audience')
      ->execute();
    $num = $all_audience_terms_from_data_table->rowCount();

    // Step 2.
    if ($num > 0) {
      db_delete('field_data_field_term_lock')
        ->condition('bundle', 'uwaterloo_audience')
        ->execute();
    }

    // Step 3.
    foreach ($all_default_term_ids as $term) {
      $data = array(
        'entity_type' => 'taxonomy_term',
        'bundle' => 'uwaterloo_audience',
        'deleted' => 0,
        'entity_id' => $term,
        'revision_id' => $term,
        'language' => 'und',
        'delta' => 0,
        'field_term_lock_value' => '1',
      );
      drupal_write_record('field_data_field_term_lock', $data);
    }
  }

  // This is for field_revision_field_term_lock table.
  if (!empty($all_default_term_ids) && db_table_exists('field_revision_field_term_lock')) {

    // Step 1.
    $all_audience_terms_from_revision_table = db_select('field_revision_field_term_lock', 'l')
      ->fields('l')
      ->condition('bundle', 'uwaterloo_audience')
      ->execute();
    $num2 = $all_audience_terms_from_revision_table->rowCount();

    // Step 2.
    if ($num2 > 0) {
      db_delete('field_revision_field_term_lock')
        ->condition('bundle', 'uwaterloo_audience')
        ->execute();
    }

    // Step 3.
    foreach ($all_default_term_ids as $term) {
      $data = array(
        'entity_type' => 'taxonomy_term',
        'bundle' => 'uwaterloo_audience',
        'deleted' => 0,
        'entity_id' => $term,
        'revision_id' => $term,
        'language' => 'und',
        'delta' => 0,
        'field_term_lock_value' => '1',
      );
      drupal_write_record('field_revision_field_term_lock', $data);
    }
  }
}

/**
 * Get the existing default taxonomy term ids of uwaterloo_audience.
 * If uwaterloo_audience has no terms, return empty array.
 * If uwaterloo_audience has the existing terms, return the default term ids which should be locked.
 */
function _uw_data_audience_categories_get_locked_terms() {

  // Get taxonomy term objects from uwaterloo_audience.
  $vocabulary = taxonomy_vocabulary_machine_name_load('uwaterloo_audience');
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));

  $default_locked_term_ids = array();

  // The default term names which should be locked.
  $default_locked_terms = array(
    "Current students",
    "Current undergraduate students",
    "Current graduate students",
    "Future students",
    "Future undergraduate students",
    "Future graduate students",
    "Faculty",
    "Staff",
    "Alumni",
    "Parents",
    "Donors | Friends | Supporters",
    "Employers",
    "International",
    "Media",
  );

  // If uwaterloo_audience vocab has the existing terms, get the default term ids.
  if (!empty($terms)) {
    foreach ($terms as $term) {
      if (in_array($term->name, $default_locked_terms)) {
        $default_locked_term_ids[] = $term->tid;
      }
    }
  }
  return $default_locked_term_ids;
}
